

**Spencer Files**

* make_folds.ipynb 

The EfficinetNetB0 combined label model

* load_data_folds.ipynb 

* Catheter.ipynb 

The EfficinetNetB0 Distributed model

* load_data_multi_models.ipynb 

* Catheter_Multiple.ipynb 


**Tina Files**

The CNN models in CNN folder

* load_data_CNN_500.ipynb

* Catheter_CNN_500.ipynb

* load_data_CNN_5411.ipynb

* Catheter_CNN_5411.ipynb



